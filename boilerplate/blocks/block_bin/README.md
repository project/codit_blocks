This is where all the block definitions go that accompany the blocks created
within Codit Blocks.

Templates reside within block directory. Even if you plan to place a copy in
your theme directory, you need a tpl for each block to register the template
correctly.

See the main README.md in codit_blocks or the help page for directions on
getting started.
